package org.kite9.diagram.builders.krmodel;

/**
 * Models the subject or object in a sentence.
 * 
 * @author moffatr
 *
 */
public interface NounPart {
   
    Object getRepresented();
    
    
    
}
