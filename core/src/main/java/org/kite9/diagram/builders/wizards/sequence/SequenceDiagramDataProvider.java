package org.kite9.diagram.builders.wizards.sequence;

import java.util.List;

public interface SequenceDiagramDataProvider {

	public List<Step> getSteps();
	
}
