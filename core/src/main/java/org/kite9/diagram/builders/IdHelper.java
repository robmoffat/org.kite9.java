package org.kite9.diagram.builders;

/** 
 * Returns Ids to use for the ADL elements from a given object.
 * 
 * @author robmoffat
 *
 */
public interface IdHelper {

	public String getId(Object o);
}