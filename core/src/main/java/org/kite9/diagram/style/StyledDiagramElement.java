package org.kite9.diagram.style;

public interface StyledDiagramElement {

	public String getStyle();
	
	public void setStyle(String s);
	
	public String getClasses();
	
	public void setClasses(String s);
}
