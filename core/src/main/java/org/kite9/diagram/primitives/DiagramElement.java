package org.kite9.diagram.primitives;

/**
 * Parent class for all elements of the diagram
 */
public interface DiagramElement extends Comparable<DiagramElement>{


}