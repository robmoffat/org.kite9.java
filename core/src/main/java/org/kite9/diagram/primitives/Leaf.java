package org.kite9.diagram.primitives;

/**
 * An item which is not a container of further connected items.
 * @see Container
 * 
 * @author robmoffat
 *
 */
public interface Leaf extends IdentifiableDiagramElement {

}
